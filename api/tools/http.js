export default class Http {

  static response(res, data, code = 200, status = 'OK') {
    return res.status(code).json({message: status, data: data})
  }

  static responseError(res, errorMessage, code = 400, status = 'ERROR') {
    return res.status(code).json({message: status, data: errorMessage})
  }
}