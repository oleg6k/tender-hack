export default class BaseController {

  constructor(req, res) {
    this.req = req;
    this.res = res
  }


  async responseJson(code = 200, status = 'OK', data = {}) {
    let response = {
      status: status,
      data: data,
    };
    return this.res.status(code).json(response)
  }
}