import http from "../../tools/http";
import ProductService from "../../services/ProductService";
import MetricService from "../../services/MetricService";

class IndexController {

  static async getAllProducts(req, res) {
    ProductService.getAllProducts()
      .then(products => {
        http.response(res, products)
      })
      .catch(e => {
        http.responseError(res, e.message)
      })
  }

  static async getPredictionForProduct(req, res) {
    try{
      let product = await ProductService.getProductById(req.body.product_id)
      let metric = await MetricService.getClosestMetric(product)
      let points = await MetricService.calculateNextThreePoint(product, metric)
      return http.response(res, {
        product: product,
        metric: metric,
        points: points
      })
    }catch (e) {
      return http.responseError(res, e.message)
    }
  }

}

export default IndexController;
