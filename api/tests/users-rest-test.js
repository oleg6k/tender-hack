import chai from 'chai';
import chatHttp from 'chai-http';
import 'chai/register-should';
import app from '../index';

chai.use(chatHttp);
const {expect} = chai;

describe('Testing the user endpoints:', () => {
  //TODO: off
  it('It should create a user', (done) => {
    const user = {
      name: 'Test',
      email: 'test2@test2.test',
      phone: '+74984186284',
      surname: 'Testov'
    };
    chai.request(app)
      .post('/api/v1/users')
      .set('Accept', 'application/json')
      .send(user)
      .end((err, res) => {
        expect(res.status).to.equal(201);
        expect(res.body.data).to.include({
          name: 'Test',
          email: 'test2@test2.test',
          phone: '+74984186284',
          surname: 'Testov'
        });
        done();
      });
  });

  //TODO: off
  it('It should not create a user with incomplete parameters', (done) => {
    const user = {
      name: 'Test',
      email: 'test@test.test'
    };
    chai.request(app)
      .post('/api/v1/users')
      .set('Accept', 'application/json')
      .send(user)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it('It should get all users', (done) => {
    chai.request(app)
      .get('/api/v1/users')
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        res.body.data[0].should.have.property('name');
        res.body.data[0].should.have.property('email');
        res.body.data[0].should.have.property('phone');
        res.body.data[0].should.have.property('surname');
        done();
      });
  });

  it('It should get a particular user', (done) => {
    const userId = 3;
    chai.request(app)
      .get(`/api/v1/users/${userId}`)
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        res.body.data.should.have.property('id');
        res.body.data.should.have.property('email');
        res.body.data.should.have.property('phone');
        res.body.data.should.have.property('name');
        done();
      });
  });

  it('It should not get a particular user with invalid id', (done) => {
    const userId = 500;
    chai.request(app)
      .get(`/api/v1/users/${userId}`)
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(res.status).to.equal(404);
        res.body.should.have.property('message');
        //.eql(`Cannot find user with the id ${userId}`);
        done();
      });
  });

  it('It should not get a particular user with non-numeric id', (done) => {
    const userId = 'aaa';
    chai.request(app)
      .get(`/api/v1/users/${userId}`)
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(res.status).to.equal(400);
        res.body.should.have.property('message')
          .eql('Please input a valid numeric value');
        done();
      });
  });

  //TODO: off
  it('It should update a user', (done) => {
    const userId = 3;
    const updateduser = {
      id: userId,
      name: 'Test-rename'
    };
    chai.request(app)
      .put(`/api/v1/users/${userId}`)
      .set('Accept', 'application/json')
      .send(updateduser)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.data.id).equal(updateduser.id);
        expect(res.body.data.name).equal(updateduser.name);
        done();
      });
  });

  //TODO: off
  it('It should not update a user with invalid id', (done) => {
    const userId = '9999';
    const updateduser = {
      id: userId,
      name: 'Test-rename'
    };
    chai.request(app)
      .put(`/api/v1/users/${userId}`)
      .set('Accept', 'application/json')
      .send(updateduser)
      .end((err, res) => {
        expect(res.status).to.equal(404);
        res.body.should.have.property('message');
        //.eql(`Cannot find user with the id: ${userId}`);
        done();
      });
  });

  //TODO: off
  it('It should not update a user with non-numeric id value', (done) => {
    const userId = 'ggg';
    const updateduser = {
      id: userId,
      name: 'Test-rename'
    };
    chai.request(app)
      .put(`/api/v1/users/${userId}`)
      .set('Accept', 'application/json')
      .send(updateduser)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        res.body.should.have.property('message')
          .eql('Please input a valid numeric value');
        done();
      });
  });


  // //TODO: off
  // it('It should delete a user', (done) => {
  //   const userId = 1;
  //   chai.request(app)
  //     .delete(`/api/v1/users/${userId}`)
  //     .set('Accept', 'application/json')
  //     .end((err, res) => {
  //       expect(res.status).to.equal(200);
  //       expect(res.body.data).to.include({});
  //       done();
  //     });
  // });

  // //TODO: off
  // it('It should not delete a user with invalid id', (done) => {
  //   const userId = 777;
  //   chai.request(app)
  //     .delete(`/api/v1/users/${userId}`)
  //     .set('Accept', 'application/json')
  //     .end((err, res) => {
  //       expect(res.status).to.equal(404);
  //       res.body.should.have.property('message')
  //         .eql(`user with the id ${userId} cannot be found`);
  //       done();
  //     });
  // });

  // //TODO: off
  // it('It should not delete a user with non-numeric id', (done) => {
  //   const userId = 'bbb';
  //   chai.request(app)
  //     .delete(`/api/v1/users/${userId}`)
  //     .set('Accept', 'application/json')
  //     .end((err, res) => {
  //       expect(res.status).to.equal(400);
  //       res.body.should.have.property('message').eql('Please provide a numeric value');
  //       done();
  //     });
  // });

});
