```bash
npm i
```

```bash
npm i -g sequelize-cli pg nodemon
```

```bash
sequelize db:migrate
```

```bash
sequelize db:seed:all
```

```bash
npm start
```