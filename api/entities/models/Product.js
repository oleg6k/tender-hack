'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    }
  }, {
    sequelize,
    underscored: true
  });
  Product.associate = function (models) {

    Product.hasMany(models.ProductHistory, {
      as: 'product_histories',
      foreignKey: {
        name: 'product_id'
      }
    });


  };
  return Product;
};