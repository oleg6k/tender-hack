'use strict';
module.exports = (sequelize, DataTypes) => {
  const MetricHistory = sequelize.define('MetricHistory', {
    metric_id: DataTypes.INTEGER,
    value: DataTypes.FLOAT,
    timestamp:
      {
        type: DataTypes.DATE,
        allowNull: false,
        // get() {
        //   return this.getDataValue('timestamp').getTime()
        // }
      },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    }
  }, {
    sequelize,
    underscored: true
  });
  MetricHistory.associate = function (models) {
    // associations can be defined here
  };
  return MetricHistory;
};