'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProductHistory = sequelize.define('ProductHistory', {
    product_id: DataTypes.INTEGER,
    value: DataTypes.FLOAT,
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      // get() {
      //   return this.getDataValue('timestamp').getTime()
      // }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    }
  }, {
    sequelize,
    underscored: true
  });

  ProductHistory.prototype.timestamp_raw = function  timestamp_raw(){
    return this.timestamp
  }

  ProductHistory.associate = function (models) {
    // associations can be defined here
  };
  return ProductHistory;
};