'use strict';
module.exports = (sequelize, DataTypes) => {
  const Metric = sequelize.define('Metric', {
    name: DataTypes.STRING,
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date()
    }
  }, {
    sequelize,
    underscored: true
  });
  Metric.associate = function (models) {


    Metric.hasMany(models.MetricHistory, {
      as: 'metric_histories',
      foreignKey: {
        name: 'metric_id'
      }
    });


  };
  return Metric;
};