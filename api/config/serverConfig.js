require('dotenv').config();
module.exports = {
  api: {
    url: process.env.APP_URL,
    port: process.env.APP_PORT,
  }
};