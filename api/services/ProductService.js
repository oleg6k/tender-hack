import db from '../entities/models';

class ProductService {

  static async addProduct(product) {
    try {
      return await db.Product.create(product);
    } catch (error) {
      throw error;
    }
  }

  static async getProductById(productId) {
    try {
      return await db.Product.findOne(
        {
          where: {id: Number(productId)},
          include: [
            {model: db.ProductHistory, as: 'product_histories', nested: true},
          ]
        });
    } catch (error) {
      throw error;
    }
  }

  static async getAllProducts() {
    try {
      return await db.Product.findAll({
        include: [
          {model: db.ProductHistory, as: 'product_histories', nested: true},
        ]
      });
    } catch (error) {
      throw error;
    }
  }
}

export default ProductService