import db from '../entities/models';

const Op = db.Sequelize.Op

class MetricService {

  static async addMetric(metric) {
    try {
      return await db.Metric.create(metric);
    } catch (error) {
      throw error;
    }
  }

  static async getMetricById(metricId) {
    try {
      return await db.Metric.findOne(
        {
          where: {id: Number(metricId)},
          include: [
            {model: db.MetricHistory, as: 'metric_histories', nested: true},
          ]
        });
    } catch (error) {
      console.log('getMetricById error', error.message)
      throw error;
    }
  }

  static async getAllMetrics() {
    try {
      return await db.Metric.findAll({
        include: [
          {model: db.MetricHistory, as: 'metric_histories', nested: true},
        ]
      });
    } catch (error) {
      throw error;
    }
  }

  static async getClosestMetric(product) {
    try {
      const productTimestamps = product.product_histories.map(history => history.timestamp)

      console.log('productTimestamps', productTimestamps)
      const allMetrics = await db.Metric.findAll({
        include: [
          {
            model: db.MetricHistory,
            as: 'metric_histories',
            nested: true,
            where: {
              timestamp: {[Op.in]: productTimestamps}
            }
          }]
      })

      const results = await Promise.all(allMetrics.map(async (metric) => {
        let resTemplate = {
          metric_id: metric.id,
          deltas: [],
          delta: null
        }

        let metricPoints = metric.metric_histories
        let productPoints = await this.getProductPoints(product, metricPoints)
        productPoints = productPoints.filter(function (e) {
          return e !== undefined && typeof e !== 'undefined'
        });
        let productsDeltas = this.cleanUpArray(await this.getPointDeltas(productPoints))
        let metricDeltas = this.cleanUpArray(await this.getPointDeltas(metricPoints))
        resTemplate.deltas = this.cleanUpArray(await this.getResultDeltas(productsDeltas, metricDeltas))
        resTemplate.delta = resTemplate.deltas.reduce((a, b) => (a + b)) / resTemplate.deltas.length;

        return resTemplate

      }))

      const metricId = await this.getClosestMetricId(results)

      return await this.getMetricById(metricId)

    } catch (error) {

      throw error;
    }
  }

  static async calculateNextThreePoint(product, metric) {

    let mHistories = metric.metric_histories
    let pHistories = product.product_histories

    let firstMetricHistory = mHistories[mHistories.length - 3]
    let secondMetricHistory = mHistories[mHistories.length - 2]
    let thirdMetricHistory = mHistories[mHistories.length - 1]

    let lastProductHistory = pHistories[pHistories.length - 1]

    let deltas = this.cleanUpArray(await this.getPointDeltas([firstMetricHistory, secondMetricHistory, thirdMetricHistory]))
    return Promise.all(
      deltas.map((delta, i) => {
        lastProductHistory.value = lastProductHistory.value * (1 + delta)
        let nextPoint = new Date();
        nextPoint.setDate(nextPoint.getDate()+ (i*2) + 2);
        return [nextPoint, lastProductHistory.value]
      })
    )
  }

  static cleanUpArray(array) {
    return array.filter(function (e) {
      return e !== undefined && typeof e !== 'undefined'
    });
  }

  static async getPointDeltas(points) {
    return Promise.all(
      points.map((point, i) => {
        if (points[i + 1]) {
          return (points[i + 1].value - point.value) / point.value
        }
      })
    )
  }

  static async getProductPoints(product, metricPoints) {
    return Promise.all(
      metricPoints.map((metricHistory) => {
        return product.product_histories.find(pHistory => {
          return pHistory.timestamp.toString() === metricHistory.timestamp.toString()
        })
      })
    )
  }

  static async getResultDeltas(productDeltas, metricDeltas) {
    return Promise.all(
      productDeltas.map((productDelta, i) => {
        if (metricDeltas[i]) {
          return Math.abs(productDelta / metricDeltas[i])
        }
      })
    )
  }

  static async getClosestMetricId(results) {

    let goal = 1
    let deltas = results.map((result, i) => result.delta)
    let closest = deltas.reduce(function (prev, curr) {
      return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
    });
    return results[deltas.indexOf(closest)].metric_id
  }

}

export default MetricService