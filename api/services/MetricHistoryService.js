import db from '../entities/models';

class MetricHistoryService {

  static async addMetricHistory(metricHistory) {
    try {
      return await db.MetricHistory.create(metricHistory);
    } catch (error) {
      throw error;
    }
  }

  static async getMetricHistoryById(metricHistoryId) {
    try {
      return await db.MetricHistory.findOne({where: {id: Number(metricHistoryId)}});
    } catch (error) {
      throw error;
    }
  }

  static async getAllMetricHistories() {
    try {
      return await db.MetricHistory.findAll();
    } catch (error) {
      throw error;
    }
  }
}

export default MetricHistoryService