import db from '../entities/models';

class ProductHistoryService {

  static async addProductHistory(productHistory) {
    try {
      return await db.ProductHistory.create(productHistory);
    } catch (error) {
      throw error;
    }
  }

  static async getProductHistoryById(productHistoryId) {
    try {
      return await db.ProductHistory.findOne({where: {id: Number(productHistoryId)}});
    } catch (error) {
      throw error;
    }
  }

  static async getAllProductHistories() {
    try {
      return await db.ProductHistory.findAll();
    } catch (error) {
      throw error;
    }
  }
}

export default ProductHistoryService