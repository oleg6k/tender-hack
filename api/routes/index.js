import IndexController from "../controllers/admin/IndexController";
const router = require('express').Router();

router.get('/', IndexController.getAllProducts);
router.post('/', IndexController.getPredictionForProduct);

module.exports = router;