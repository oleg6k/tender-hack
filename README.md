## tender-hack

#### docker:

```bash
docker volume create --name=postgres_hack
```

```bash
docker-compose up -d --build
```

##### migrations
```bash
docker-compose exec api sequelize db:migrate
```
##### seeds
```bash
docker-compose exec api sequelize db:seed:all
```

#### GO TO 
http://localhost:1012

#### API
http://localhost:1012/api



