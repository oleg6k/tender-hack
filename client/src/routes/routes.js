import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import {urls} from './urls'
import IndexComponent from "../components/views/IndexComponent/IndexComponent";
import HeaderFooterContainer from '../components/layouts/HeaderFooterContainer';

export default class Routes extends Component {
  render() {
    return (
      <Switch>
        <HeaderFooterContainer>
          <Route exact path={`${urls.main}`} render={(props) => (
            <IndexComponent {...props}/>
          )}/>
        </HeaderFooterContainer>
      </Switch>
    );
  }
}