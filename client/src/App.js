import React from 'react';
import './App.css';
import MainRoutes from './routes/routes'
import { Route, Switch } from 'react-router-dom';


class App extends React.Component {
  render() {
    return (
      <div className='app' id='app'>
        <Switch>
          <Route path="/" render={(props) => (
            <MainRoutes {...props} />
          )} />
        </Switch>
      </div>
    )
  }
}

export default App;
