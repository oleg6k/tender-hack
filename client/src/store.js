import {RouterStore} from 'mobx-react-router';
import {MainStore} from './store/MainStore';

const mainStore = new MainStore();

export const store = {
  mainStore,
  routing: new RouterStore(),
};
