import {action, computed, observable} from 'mobx';
import requests from "../requests";

export class MainStore {

  @observable
  loading = false;
  @observable
  products = [];
  @observable
  prediction = {};
  @observable
  productName = '';
  @observable
  metricName = '';
  @observable
  newPoints = [];

  @computed
  get predictionProduct() {
    return this.prediction.product ? this.prediction.product : null
  }

  @computed
  get productCoordinates() {
    return this.predictionProduct ?
      this.predictionProduct.product_histories.map((productHistory) => [new Date(productHistory.timestamp).getTime(),productHistory.value]) :
      []
  }

  @computed
  get predictionProductCoordinates() {
    if (this.predictionProduct) {
      let coordinates = this.predictionProduct.product_histories.map((productHistory) => [new Date(productHistory.timestamp).getTime(),productHistory.value])
      coordinates = coordinates.concat(this.predictionPointCoordinates)
      return coordinates
    } else {
      return []
    }
  }

  @computed
  get predictionMetricCoordinates() {
    return this.prediction.metric ?
      this.prediction.metric.metric_histories.map((metricHistory) => [new Date(metricHistory.timestamp).getTime(), metricHistory.value]) :
      []
  }

  @computed
  get predictionPointCoordinates() {
    return this.prediction.points ?
      this.prediction.points.map((point) => [new Date(point[0]).getTime(), point[1]]) :
      []
  }

  @action.bound
  async loadAllProducts() {
    this.loading = true;
    this.error = false;
    try {
      const response = await requests.products.getAllProducts();
      this.products = response.data.data;
    } catch (e) {
      this.error = true;
    } finally {
      this.loading = false;
    }
  }

  @action.bound
  async getPredictionForProduct(productId) {
    this.loading = true;
    this.error = false;
    try {
      const response = await requests.products.getPredictionForProduct({product_id: productId})
      this.prediction = response.data.data;
      this.productName = this.prediction.product.name
      this.metricName = this.prediction.metric.name
      this.newPoints = this.prediction.points
    } catch (e) {
      this.error = true;
    } finally {
      this.loading = false;
    }
  }

}