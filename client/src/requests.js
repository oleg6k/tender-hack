import axios from 'axios'

export const instance = axios.create()

instance.interceptors.request.use(request => {

  return request;
});

instance.interceptors.response.use(
  response => {

    return response;
  },
  error => {

    return Promise.reject(error)
  }
)

export default {
  products: {
    getAllProducts() {
      return instance.get('/api/')
    },
    getPredictionForProduct(data) {
      return instance.post('/api/', data)
    }
  }
}