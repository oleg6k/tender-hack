import React from 'react';
import ReactEcharts from 'echarts-for-react';
import {inject, observer} from 'mobx-react';
import ecStat from 'echarts-stat';

@inject('mainStore')
@observer
class GraphComponent extends React.Component {
  constructor(props) {
    super(props)
  }

  getOption = () => {

    let data = this.props.coordinates
    let myRegression = ecStat.regression('polynomial', data, 5);

    myRegression.points.sort(function(a, b) {
      return a[0] - b[0];
    });


    return {

      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross'
        }
      },
      title: {
        text: `${this.props.title}`,
        left: 'center',
        top: 16
      },
      xAxis: {
        type: 'value',
        axisLabel: {
          formatter: function (val) {
            return new Date(val).toISOString().substr(0, 10);
          }
        },
        min: myRegression.points[0][0],
        splitLine: {
          lineStyle: {
            type: 'dashed'
          }
        },
        splitNumber: 10
      },
      yAxis: {
        type: 'value',
        min: -40,
        splitLine: {
          lineStyle: {
            type: 'dashed'
          }
        }
      },
      grid: {
        top: 90
      },
      series: [{
        name: 'scatter',
        type: 'scatter',
        emphasis: {
          label: {
            show: true,
            position: 'right',
            color: 'blue',
            fontSize: 16
          }
        },
        data: data
      }, {
        name: 'line',
        type: 'line',
        smooth: true,
        showSymbol: false,
        data: myRegression.points,
        markPoint: {
          itemStyle: {
            color: 'transparent'
          },
          label: {
            show: true,
            position: 'left',
            color: '#333',
            fontSize: 14
          },
          data: [{
            coord: myRegression.points[myRegression.points.length - 1]
          }]
        }
      }]
    };
  }



  render() {
    return (
      <ReactEcharts option={this.getOption()}/>
    )
  }
}

export default GraphComponent