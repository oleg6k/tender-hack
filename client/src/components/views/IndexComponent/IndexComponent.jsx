import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Card, Dropdown} from 'semantic-ui-react'
import GraphComponent from "../GraphComponent/GraphComponent";

@inject('mainStore')
@observer
class IndexComponent extends React.Component {
  constructor(props) {
    super(props)

  }

  componentDidUpdate(prevProps, prevState, snapshot) {

  }

  renderProductGraphComponent() {
    if (this.props.mainStore.predictionProduct) {
      return (
        <GraphComponent
          coordinates={this.props.mainStore.productCoordinates}
          title={'Текущий: '+ this.props.mainStore.productName}/>
      )
    }
    return (<div></div>)
  }


  renderPredictProductGraphComponent() {
    if (this.props.mainStore.predictionProduct) {
      return (<GraphComponent coordinates={this.props.mainStore.predictionProductCoordinates}
                              title={'Прогноз: '+ this.props.mainStore.productName}/>)
    }
    return (<div></div>)
  }

  renderPredictMetricGraphComponent() {
    if (this.props.mainStore.predictionProduct) {
      return (<GraphComponent coordinates={this.props.mainStore.predictionMetricCoordinates}
                              title={'Метрика: '+this.props.mainStore.metricName}
      />)
    }
    return (<div></div>)
  }


  onChangeProduct = async (event, target) => {
    await this.props.mainStore.getPredictionForProduct(target.value)
    return this.setState({[target.name]: target.value})
  }


  render() {
    const productOptions = this.props.mainStore.products.map((product, i) => {
      return {
        key: i,
        text: product.name,
        value: product.id
      }
    })

    return (
      <Card fluid>
        <Card.Content>
          <Button onClick={this.props.mainStore.loadAllProducts}>Refresh</Button>
        </Card.Content>
        <Card.Content>
          <Dropdown
            placeholder='Select product'
            fluid
            selection
            name="product_id"
            options={productOptions}
            onChange={this.onChangeProduct}/>
        </Card.Content>
        <Card.Content>
          {this.renderProductGraphComponent()}
        </Card.Content>
        <Card.Content>
          {this.renderPredictProductGraphComponent()}
        </Card.Content>
        <Card.Content>
          {this.renderPredictMetricGraphComponent()}
        </Card.Content>
      </Card>
    )
  }
}

export default IndexComponent