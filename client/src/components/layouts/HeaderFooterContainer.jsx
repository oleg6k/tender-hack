import React from 'react';
import HeaderComponent from "./HeaderComponent/HeaderComponent";
import {FooterComponent} from "./FooterComponent/FooterComponent";

export default class HeaderFooterContainer extends React.Component {
  render() {
    return (
      <div>
        <HeaderComponent/>
        {this.props.children}
        <FooterComponent/>
      </div>
    )
  }
}