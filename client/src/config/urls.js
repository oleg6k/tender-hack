export const urls = {
  landing: '/',

  // BACKEND URLS
  apiUrl: process.env.REACT_APP_API_ENDPOINT,
  appUrl: process.env.REACT_APP_APP_URL,

}