const Dotenv = require('dotenv-webpack');

module.exports = {
  entry: {
    main: ["babel-polyfill", '../../client/src/index.js'],
    // entry: ["babel-polyfill", "./app/js"]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './dist'
  },
  plugins: [
    new Dotenv()
  ]
};